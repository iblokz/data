'use strict';

const keyValue = (k, v) => {
	let o = {};
	o[k] = v;
	return o;
};

const clone = o => Object.assign(Object.create(Object.getPrototypeOf(o) || {}), o);

const sub = (o, p) => (p instanceof Array)
	? (typeof o[p[0]] !== 'undefined' && p.length > 1) ? sub(o[p[0]], p.slice(1)) : o[p[0]]
	: o[p];

const patch = (o, k, v) => Object.assign(clone(o),
	(k instanceof Array)
		? keyValue(k[0], (k.length > 1)
			? patch(o[k[0]] || {}, k.slice(1), v)
			: typeof o[k[0]] === 'object' && o[k[0]].constructor === Object && Object.assign(clone(o[k[0]]), v) || v)
		: keyValue(k, typeof o[k] === 'object' && o[k].constructor === Object && Object.assign(clone(o[k]), v) || v)
);

const reduce = (o, reduceFn, initial) => Object.keys(o)
	.reduce(
		(accumulator, key, index) => reduceFn(accumulator, key, o[key], index, o),
		typeof initial === 'undefined'
			? o[Object.keys(o)[0]] // to confirm with the array reduce spec in this case the firts property is used
			: initial
	);

const map = (o, mapFn) => reduce(o,
	(o2, key, value, index) => patch(o2, key, mapFn(key, value, index, o)),
	{}
);

const filter = (o, filterFn) => reduce(o,
	(o2, key, value, index) => filterFn(key, value, index, o)
		? patch(o2, key, value)
		: o2,
	{}
);

const traverse = (tree, fn) => Object.keys(tree).reduce((o, k) =>
	patch(o, k,
		(typeof tree[k] === 'object' && tree[k].constructor === Object)
			? traverse(tree[k], fn)
			: fn(tree[k], k)
	), {}
);

const chainCall = (o, chain) => chain.reduce(
	(o, link) => (typeof link[1] === 'undefined')
		? o[link[0]]()
		: o[link[0]](link[1]),
	o
);

const _switch = (value, cases) =>
	sub(cases, value) && sub(cases, value)['default'] || sub(cases, value)
	|| (value instanceof Array)
		&& value.length > 1 && _switch(value.slice(0, value.length - 1), cases)
	|| cases['default'] || false;

module.exports = {
	keyValue,
	clone,
	sub,
	patch,
	reduce,
	map,
	filter,
	traverse,
	chainCall,
	switch: _switch
};
